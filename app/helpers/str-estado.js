import Ember from 'ember';

export function strEstado(task, hash) {
    task = task[0];

    if (task.finished) {
        return "Finalizado";

    } else if (hash.enabled) {
        return "Habilitado";
    }

    return "Deshabilitado";
}

export default Ember.HTMLBars.makeBoundHelper(strEstado);
