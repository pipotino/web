import Ember from 'ember';

export default Ember.View.extend({

    didInsertElement: function (){
        Em.$('[data-toggle="tooltip"]').tooltip();
    }
});
