import Em from "ember";
import DS from "ember-data";
import Session from "simple-auth/session";

var TesisSession = Session.extend({
    user: null,

    userObs: function() {
        var self = this;

        Em.$.getJSON(Web.API_HOST + "users/logged").then(
            function (user) {
                self.set("user", user);
            },
            Em.$.noop
        );
    }.observes('token')
});

export default {
    name: 'tesis-session',
    before: "simple-auth",
    initialize: function initialize(container, application) {
        application.register('session:tesis', TesisSession);
    }
};
