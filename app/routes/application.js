/* Ruta par application
 * Se encarga de manejar los eventos de `session`
 */

import ApplicationRouteMixin from 'simple-auth/mixins/application-route-mixin';
import Em from 'ember';

export default Em.Route.extend(ApplicationRouteMixin);
