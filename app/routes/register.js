import Em from 'ember';

export default Em.Route.extend({

    /* If user is already auth, nothing to do here */
    beforeModel: function () {
        this._super(arguments);
        if (this.get("session.isAuthenticated")) {
            this.transitionTo("index");
        }
    }
});
