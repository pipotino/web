import Ember from 'ember';

export default Ember.Route.extend({
    setupController: function () {
        if (this.controller.get("user") === null) {
            this.transitionTo("login");
        }
    }
});
