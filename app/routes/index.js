import Em from 'ember';
import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin';

export default Em.Route.extend(AuthenticatedRouteMixin, {
    setupController: function (controller, model) {
        if (!this.get("session.isAuthenticated")) {
            this.transitionTo("login");
            return;
        }

        Em.$.getJSON(Web.API_HOST + "tasks").then(
            function (response) {
                controller.set("tasks", response);
                Em.run.scheduleOnce("afterRender", function () {
                    Em.$('[data-toggle="tooltip"]').tooltip();
                });
            },

            function (response) {
                console.error("cannot connect to server", response);
            }
        );

    }
});
