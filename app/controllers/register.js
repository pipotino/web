/* controller de register */
import Ember from 'ember';

// TODO: Si esta logueado que vaya al index

export default Ember.Controller.extend({
    name: null,
    lastname: null,
    username: null,
    password: null,
    error: null,  /* error message to display */

    actions: {
        register: function () {
            var self = this;

            Ember.$.post("http://codingways.com:8080/register", {
                name: this.get("name"),
                lastname: this.get("lastname"),
                username: this.get("username"),
                password: this.get("password")

            }, function () { // success
                self.transitionToRoute("login");

            }, "json").fail(function (response) {
                console.error(response);
                self.set("error", response.responseJSON.message);
            });
        }
    }
});
