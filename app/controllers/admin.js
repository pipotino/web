import Ember from 'ember';

export default Ember.Controller.extend({
    needs: ['application'],
    user: Ember.computed.alias(
        'controllers.application.user')

});
