import Ember from 'ember';

// TODO: Si esta logueado que vaya al index

export default Ember.Controller.extend({

    username: null,
    password: null,
    error: null,  /* error message to display */

    actions: {
        login: function () {
            var self = this;

            this.get("session").authenticate(
                'simple-auth-authenticator:token', {
                    identification: this.get("username"),
                    password: this.get("password")
                }
            ).then(
                Ember.$.noop,
                function (response) {
                    console.error(response);
                    self.set("error", response.message);
                }
            );
        }
    }
});
