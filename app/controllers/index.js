import Em from 'ember';

export default Em.Controller.extend({

    /* array of tasks... */
    tasks: null,
    result: null,
    result_id: null,

    current: {
        task: null,
        index: null
    },

    /* add more data */
    moreData: "",

    actions: {
        /* cierra la session */
        doLogout: function () {
            this.get("session").invalidate();
        },

        doAgregarDatos: function (task, index) {
            this.set("current.task", task);
            this.set("current.index", index);
        },

        /* Envia un habilitar al server */
        doHabilitar: function (task, index) {
            var self = this;
            Em.$.post(Web.API_HOST + "tasks/" + task._id + "/enable").
                then(function () {
                    Em.set(self.get("tasks"),
                           index.toString() + ".enabled_to_process", true);

                }, function () {
                    console.error("fail", arguments);
                });
        },

        /* Elimina una tarea del server y de la tabla */
        doEliminar: function (task, index) {
            var self = this;
            Em.$.ajax({
                method: "delete",
                url: Web.API_HOST + "tasks/" + task._id
            }).then(function () {
                self.get("tasks").popObject(index);
            }, function () {
                console.error(arguments);
            });
        },

        /* Muestra los resultados */
        doResultados: function (task_id) {
            var self = this;
            if (self.get("result_id") === task_id) {
                self.set("result_id", null);
                self.set("result", null);
                return;
            }

            Em.$.getJSON(Web.API_HOST + "task_results/" + task_id).then(
                function (result) {
                    self.set("result_id", task_id);
                    var printed = "";
                    for (var key in result) {
                        if (result.hasOwnProperty(key)) {
                            printed += key + ": " + result[key];
                        }
                    }

                    self.set("result", printed);
                }, function () {
                console.error(arguments);
            });
        },

        /* Es el guardar del modela */
        doMoreData: function () {
            var data = this.get("moreData"),
                json,
                task = this.get("current.task");

            try {
                json = JSON.parse(data);

                Em.$.post(
                    Web.API_HOST + "tasks/" + task._id + "/addData",
                    json,
                    "json"
                ).then(
                    function (response) {
                        // TODO: sum available_slices
                        console.log("Ok", response);
                        Em.$("#addTaskData").modal("hide");
                    },
                    function () {
                        console.error(arguments);
                        Em.$("#addTaskData").modal("hide");

                    });

            } catch (ex) {
                console.error(ex);
            }
        },

        doAddTask: function () {
            var map = this.get("newMap"),
                reduce = this.get("newReduce"),
                data = this.get("newData"),
                self = this;

            try {
                data = JSON.parse(data);
            } catch (ex) {
                console.log(ex);
                return;
            }

            Em.$.post(
                Web.API_HOST + "tasks", {
                    imap: map,
                    ireduce: reduce,
                    slices: data
                }, "json"
            ).then(
                function (task) {
                    self.get("tasks").addObject(task);
                    Em.$("#taskForm").modal("hide");
                },
                function () {
                    console.log(arguments);
                    Em.$("#taskForm").modal("hide");
                }
            );
        }
    }

});
