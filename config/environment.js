/* jshint node: true */

module.exports = function(environment) {
    var ENV = {
        modulePrefix: 'web',
        environment: environment,
        baseURL: '/',
        locationType: 'history',
        EmberENV: {
            FEATURES: {
                // Here you can enable experimental features on an ember canary build
                // e.g. 'with-controller': true
            }
        },

        APP: {
            // Here you can pass flags/options to your application instance
            // when it is created
        }
    };

    if (environment === 'development') {
        ENV.APP.API_HOST = "http://tesis.codingways.com:8080/api/v1/";
        ENV.APP.API_NAMESPACE = "";
        ENV['simple-auth'] = {
            // // routeAfterAuthentication: "crystal.index",
            routeAfterAuthentication: "index",
            authorizer: 'simple-auth-authorizer:token',
            session: 'session:tesis',
            serverTokenEndpoint: "http://tesis.codingways.com:8080/login",
            crossOriginWhitelist: ['http://tesis.codingways.com:8080/, http://127.0.0.1:4200', 'http://127.0.0.1:9000', 'http://localhost:9000', 'http://localhost:4200']
        };

        ENV['simple-auth-token'] = {
            serverTokenEndpoint: "http://tesis.codingways.com:8080/login",
            identificationField: 'username',
            passwordField: 'password',
            tokenPropertyName: 'token',
            authorizationPrefix: 'Bearer ',
            authorizationHeaderName: 'Authorization',
            headers: {}

        };


        // ENV.APP.LOG_RESOLVER = true;
        // ENV.APP.LOG_ACTIVE_GENERATION = true;
        // ENV.APP.LOG_TRANSITIONS = true;
        // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
        // ENV.APP.LOG_VIEW_LOOKUPS = true;
    }

    if (environment === 'test') {
        // Testem prefers this...
        ENV.baseURL = '/';
        ENV.locationType = 'none';

        // keep test console output quieter
        ENV.APP.LOG_ACTIVE_GENERATION = false;
        ENV.APP.LOG_VIEW_LOOKUPS = false;

        ENV.APP.rootElement = '#ember-testing';
    }

    if (environment === 'production') {

    }

    return ENV;
};
